from django import forms
from online.models import Student, Teacher, Course, Inventory, Week
from online.lib import form_cgi as fc


class Student_Form(forms.ModelForm):
    class Meta:
        model = Student
        fields = '__all__'


class Teacher_Form(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = '__all__'


class Course_Form(forms.ModelForm):
    class Meta:
        model = Course
        fields = '__all__'


class Week_Form(forms.ModelForm):
    class Meta:
        model = Week
        fields = '__all__'


class DateInput(forms.DateInput):
    input_type = 'date'


class Inventory_Form(forms.ModelForm):
    class Meta:
        model = Inventory
        fields = '__all__'


class Result_Week_Form(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        selection = fc.get_week_selection()
        self.fields['week'] = forms.ChoiceField(choices=selection, label='week')
        self.fields['week'].initial = '---'
