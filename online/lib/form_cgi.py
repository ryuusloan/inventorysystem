from online.models import Week


def get_week_selection():
    weeks = Week.objects.all()
    selection = [[each.id, "{} ~ {}".format(each.week_start_date, each.week_stop_date)] for each in weeks]
    selection.insert(0, ['---', '---'])
    return selection
