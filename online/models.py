from django.db import models


# Create your models here.
class Student(models.Model):
    student = models.CharField(max_length=20)
    
    def __str__(self):
        return self.student


class Teacher(models.Model):
    teacher = models.CharField(max_length=20)
    email = models.EmailField(blank=True)

    def __str__(self):
        return self.teacher


class Course(models.Model):
    MONDAY = '週一'
    TUESDAY = '週二'
    WEDNESDAY = '週三'
    THURSDAY = '週四'
    FRIDAY = '週五'
    SATURDAY = '週六'
    SUNDAY = '週日'
    Week_Days = (
        (MONDAY, 'Monday'),
        (TUESDAY, 'Tuesday'),
        (WEDNESDAY, 'Wednesday'),
        (THURSDAY, 'Thursday'),
        (FRIDAY, 'Friday'),
        (SATURDAY, 'Saturday'),
        (SUNDAY, 'Sunday'),
    )

    course = models.CharField(max_length=30)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    room = models.CharField(max_length=20)
    credit = models.IntegerField(default=1)
    day = models.CharField(max_length=10, choices=Week_Days)

    def __str__(self):
        return self.course


class Week(models.Model):
    week = models.IntegerField()
    week_start_date = models.DateField()
    week_stop_date = models.DateField()

    def __str__(self):
        return "{} ~ {}".format(self.week_start_date, self.week_stop_date)


class Inventory(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    week = models.ForeignKey(Week, on_delete=models.CASCADE)
    attend = models.BooleanField(default=True)
