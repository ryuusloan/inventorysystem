// This is the js file for inventory
$(function () {
    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: "get",
            dataType: "json",
            beforeSend: function () {
                $("#modal-inventory").modal("show");
            },
            success: function (data) {
                $("#modal-inventory .modal-content").html(data.html_form);
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: "json",
            success: function (data) {
                if (data.form_is_valid) {
                    $("#inventory-table tbody").html(data.html_inventory_list);
                    $("#modal-inventory").modal("hide");
                }
                else {
                    $("#modal-inventory .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    }

    // Create Inventory
    $(".js-create-inventory").click(loadForm);
    $("#modal-inventory").on("submit", ".js-inventory-create-form", saveForm);

    // Update Inventory
    $("#inventory-table").on("click", ".js-update-inventory", loadForm);
    $("#modal-inventory").on("submit", ".js-inventory-update-form", saveForm);

    // Delete Inventory
    $("#inventory-table").on("click", ".js-delete-inventory", loadForm);
    $("#modal-inventory").on("submit", ".js-inventory-delete-form", saveForm);

});