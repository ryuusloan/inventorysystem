// This is the js file for Teacher Manager
$(function () {
    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: "get",
            dataType: "json",
            beforeSend: function () {
                $("#modal-teacher").modal("show");
            },
            success: function (data) {
                $("#modal-teacher .modal-content").html(data.html_form);
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: "json",
            success: function (data) {
                if (data.form_is_valid) {
                    $("#teacher-table tbody").html(data.html_teacher_list);
                    $("#modal-teacher").modal("hide");
                }
                else {
                    $("#modal-teacher .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    }

    // Create Teacher
    $(".js-create-teacher").click(loadForm);
    $("#modal-teacher").on("submit", ".js-teacher-create-form", saveForm);

    // Update Teacher
    $("#teacher-table").on("click", ".js-update-teacher", loadForm);
    $("#modal-teacher").on("submit", ".js-teacher-update-form", saveForm);

    // Delete Teacher
    $("#teacher-table").on("click", ".js-delete-teacher", loadForm);
    $("#modal-teacher").on("submit", ".js-teacher-delete-form", saveForm);

});