// This is the js file for Week
$(function () {
    var loadForm = function () {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: "get",
            dataType: "json",
            beforeSend: function () {
                $("#modal-week").modal("show");
            },
            success: function (data) {
                $("#modal-week .modal-content").html(data.html_form);
            }
        });
    };

    var saveForm = function () {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: "json",
            success: function (data) {
                if (data.form_is_valid) {
                    $("#week-table tbody").html(data.html_week_list);
                    $("#modal-week").modal("hide");
                }
                else {
                    $("#modal-week .modal-content").html(data.html_form);
                }
            }
        });
        return false;
    }

    // Create Week
    $(".js-create-week").click(loadForm);
    $("#modal-week").on("submit", ".js-week-create-form", saveForm);

    // Update Week
    $("#week-table").on("click", ".js-update-week", loadForm);
    $("#modal-week").on("submit", ".js-week-update-form", saveForm);

    // Delete Week
    $("#week-table").on("click", ".js-delete-week", loadForm);
    $("#modal-week").on("submit", ".js-week-delete-form", saveForm);

});