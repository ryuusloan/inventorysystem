from django.urls import path, re_path
from online import views as v


urlpatterns = [
    path('', v.index, name='index'),

    # Teacher Manager    
    path('teacher/', v.teacher_list, name='teacher_list'),
    path('teacher/create', v.teacher_create, name='teacher_create'),
    path('teacher/<int:pk>/update/', v.teacher_update, name='teacher_update'),
    path('teacher/<int:pk>/delete/', v.teacher_delete, name='teacher_delete'),

    # Student Manager
    path('student/', v.student_list, name='student_list'),
    path('student/create', v.student_create, name='student_create'),
    path('student/<int:pk>/update', v.student_update, name='student_update'),
    path('student/<int:pk>/delete', v.student_delete, name='student_delete'),

    # Course Manager
    path('course/', v.course_list, name='course_list'),
    path('course/create', v.course_create, name='course_create'),
    path('course/<int:pk>/update', v.course_update, name='course_update'),
    path('course/<int:pk>/delete', v.course_delete, name='course_delete'),

    # Week Manager
    path('week/', v.week_list, name='week_list'),
    path('week/create', v.week_create, name='week_create'),
    path('week/<int:pk>/update', v.week_update, name='week_update'),
    path('week/<int:pk>/delete', v.week_delete, name='week_delete'),

    # Inventory Manager
    path('inventory/', v.inventory_list, name='inventory_list'),
    path('inventory/create', v.inventory_create, name='inventory_create'),
    path('inventory/<int:pk>/update', v.inventory_update, name='inventory_update'),
    path('inventory/<int:pk>/delete', v.inventory_delete, name='inventory_delete'),

    # Result Action
    path('result/', v.inventory_result, name='inventory_result'),
    path('result/send', v.result_ajax, name='result_send'),
]