from django.shortcuts import render, render_to_response, get_object_or_404, HttpResponse
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.mail import EmailMessage, EmailMultiAlternatives
from online.models import Teacher, Student, Course, Inventory, Week
from online.forms import Teacher_Form, Student_Form, Course_Form, Week_Form, Inventory_Form
from online.forms import Result_Week_Form


# Create your views here.
def index(request):
    return render_to_response('ion/index.html', locals())


## Create Teacher
def teacher_list(request):
    title = 'Teachers'
    page = 'teacher_list'
    teachers = Teacher.objects.all()
    return render_to_response('ion/teacher_list.html', locals())


def save_teacher_form(request, form, template_name):
    data = {}
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            teachers = Teacher.objects.all()
            data['html_teacher_list'] = render_to_string('ion/teacher_list_part.html', locals())
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)
    

def teacher_create(request):
    if request.method == 'POST':
        form = Teacher_Form(request.POST)
    else:
        form = Teacher_Form()
    return save_teacher_form(request, form, 'ion/teacher_create_part.html')


def teacher_update(request, pk):
    teacher = get_object_or_404(Teacher, pk=pk)
    if request.method == 'POST':
        form = Teacher_Form(request.POST, instance=teacher)
    else:
        form = Teacher_Form(instance=teacher)
    return save_teacher_form(request, form, 'ion/teacher_update_part.html')


def teacher_delete(request, pk):
    teacher = get_object_or_404(Teacher, pk=pk)
    data = {}
    if request.method == 'POST':
        teacher.delete()
        data['form_is_valid'] = True
        teachers = Teacher.objects.all()
        data['html_teacher_list'] = render_to_string('ion/teacher_list_part.html', locals())
    else:
        context = {'teacher': teacher}
        data['html_form'] = render_to_string('ion/teacher_delete_part.html', context, request=request)
    return JsonResponse(data)


## Create Student
def student_list(request):
    title = 'Students'
    page = 'student_list'
    students = Student.objects.all()
    return render_to_response('ion/student_list.html', locals())


def save_student_form(request, form, template_name):
    data = {}
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            students = Student.objects.all()
            data['html_student_list'] = render_to_string('ion/student_list_part.html', locals())
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def student_create(request):
    if request.method == 'POST':
        form = Student_Form(request.POST)
    else:
        form = Student_Form()
    return save_student_form(request, form, 'ion/student_create_part.html')


def student_update(request, pk):
    student = get_object_or_404(Student, pk=pk)
    if request.method == 'POST':
        form = Student_Form(request.POST, instance=student)
    else:
        form = Student_Form(instance=student)
    return save_student_form(request, form, 'ion/student_update_part.html')


def student_delete(request, pk):
    student = get_object_or_404(Student, pk=pk)
    data = {}
    if request.method == 'POST':
        student.delete()
        data['form_is_valid'] = True
        students = Student.objects.all()
        data['html_student_list'] = render_to_string('ion/student_list_part.html', locals())
    else:
        context = {'student': student}
        data['html_form'] = render_to_string('ion/student_delete_part.html', context, request=request)
    return JsonResponse(data)


## Create Course
def course_list(request):
    title = 'Courses'
    page = 'course_list'
    courses = Course.objects.all()
    return render_to_response('ion/course_list.html', locals())


def save_course_form(request, form, template_name):
    data = {}
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            courses = Course.objects.all()
            data['html_course_list'] = render_to_string('ion/course_list_part.html', locals())
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def course_create(request):
    if request.method == 'POST':
        form = Course_Form(request.POST)
    else:
        form = Course_Form()
    return save_course_form(request, form, 'ion/course_create_part.html')


def course_update(request, pk):
    course = get_object_or_404(Course, pk=pk)
    if request.method == 'POST':
        form = Course_Form(request.POST, instance=course)
    else:
        form = Course_Form(instance=course)
    return save_course_form(request, form, 'ion/course_update_part.html')


def course_delete(request, pk):
    course = get_object_or_404(Course, pk=pk)
    data = {}
    if request.method == 'POST':
        course.delete()
        data['form_is_valid'] = True
        courses = Course.objects.all()
        data['html_course_list'] = render_to_string('ion/course_list_part.html', locals())
    else:
        context = {'course': course}
        data['html_form'] = render_to_string('ion/course_delete_part.html', context, request=request)
    return JsonResponse(data)


## Create Week
def week_list(request):
    title = 'Weeks'
    page = 'week_list'
    weeks = Week.objects.all()
    return render_to_response('ion/week_list.html', locals())


def save_week_form(request, form, template_name):
    data = {}
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            weeks = Week.objects.all()
            data['html_week_list'] = render_to_string('ion/week_list_part.html', locals())
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def week_create(request):
    if request.method == 'POST':
        form = Week_Form(request.POST)
    else:
        form = Week_Form()
    return save_week_form(request, form, 'ion/week_create_part.html')


def week_update(request, pk):
    week = get_object_or_404(Week, pk=pk)
    if request.method == 'POST':
        form = Week_Form(request.POST, instance=week)
    else:
        form = Week_Form(instance=week)
    return save_week_form(request, form, 'ion/week_update_part.html')


def week_delete(request, pk):
    week = get_object_or_404(Week, pk=pk)
    data = {}
    if request.method == 'POST':
        week.delete()
        data['form_is_valid'] = True
        weeks = Week.objects.all()
        data['html_week_list'] = render_to_string('ion/week_list_part.html', locals())
    else:
        context = {'week': week}
        data['html_form'] = render_to_string('ion/week_delete_part.html', context, request=request)
    return JsonResponse(data)


## Create Schedule
def inventory_list(request):
    title = 'Inventory'
    page = 'inventory_list'
    inventorys = Inventory.objects.all()
    return render_to_response('ion/inventory_list.html', locals())


def save_inventory_form(request, form, template_name):
    data = {}
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            inventorys = Inventory.objects.all()
            data['html_inventory_list'] = render_to_string('ion/inventory_list_part.html', locals())
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def inventory_create(request):
    if request.method == 'POST':
        form = Inventory_Form(request.POST)
    else:
        form = Inventory_Form()
    return save_inventory_form(request, form, 'ion/inventory_create_part.html')


def inventory_update(request, pk):
    inventory = get_object_or_404(Inventory, pk=pk)
    if request.method == 'POST':
        form = Inventory_Form(request.POST, instance=inventory)
    else:
        form = Inventory_Form(instance=inventory)
    return save_inventory_form(request, form, 'ion/inventory_update_part.html')


def inventory_delete(request, pk):
    inventory = get_object_or_404(Inventory, pk=pk)
    data = {}
    if request.method == 'POST':
        inventory.delete()
        data['form_is_valid'] = True
        inventorys = Inventory.objects.all()
        data['html_inventory_list'] = render_to_string('ion/inventory_list_part.html', locals())
    else:
        context = {'inventory': inventory}
        data['html_form'] = render_to_string('ion/inventory_delete_part.html', context, request=request)
    return JsonResponse(data)


## Choose week to email notice to the teacher
def inventory_result(request):
    form = Result_Week_Form()
    return render(request, 'ion/result_list.html', locals())


def result_ajax(request):
    data = {}
    if request.method == 'POST':
        form = Result_Week_Form(request.POST)
        if form.is_valid():
            week_pk = form.cleaned_data['week']
            week = Week.objects.get(pk=week_pk)
            inventorys = Inventory.objects.filter(week=week).filter(attend=False)
            for qs in inventorys:
                send_mail(request, qs)
            data['message'] = "Email has been sent!"
    return JsonResponse(data)


def send_mail(reqeust, qs):
    message = render_to_string(
        'ion/result_email.html',
        {
            'teacher': qs.course.teacher.teacher[0],
            'student': qs.student,
            'week': qs.week,
            'attend': qs.attend,
            'day': qs.course.day,
        }
    )

    mail_subject = '請老師協助線上登錄{}同學的出席狀況'.format(qs.student.student)
    to_email = qs.course.teacher.email
    cc = Teacher.objects.get(pk=1).email
    email = EmailMessage(mail_subject, message, to=[to_email], cc=[cc])
    email.content_subtype = 'html'
    return email.send()
