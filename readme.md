# Inventory System using django framework
## Get Start
> ### Download
> `git clone https://github.com/sloanryuu/inventorysystem.git`  
> `pip install -r requirements.txt`  
> ### Create require file
> #### inventory/settings/dev.py
> ```
> from .base import *
> 
>
> SECRET_KEY = '_+!m^)39z624@^d1+44o+0b67_#pq&hi%uyrua^rgbb&6c2^d6'
> DEBUG = True
> ALLOWED_HOSTS = ['127.0.0.1']
> ```
> If you want to using send email function, please add the following with your own into `inventory/settings/dev.py`
> ```
> EMAIL_USE_TLS = True
> EMAIL_HOST = ''
> EMAIL_HOST_USER = ''
> EMAIL_HOST_PASSWORD = ''
> EMAIL_PORT = 
> ```
> ### Create database
> `python manage.py makemigrations`  
> `python manage.py migrate --run-syncdb`  
> ### Enjoy the server
> `python manage.py runserver`